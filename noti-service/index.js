const postman = require('postman');
const config = require('config');

// nats.io
const namespace = 'noti_service';
const NATS_HOST = config.get('nats.host');
const NATS_PORT = config.get('nats.port');

const pc = postman.createClient({'url': `nats://${NATS_HOST}:${NATS_PORT}`}, namespace);

// event
pc.subscribe('user_service.user.created', (data, replyTo) => {
    console.log(`Send welcome email to: ${data.email}`);
});

console.log('noti-service is running');