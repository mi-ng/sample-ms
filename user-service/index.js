const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const postman = require('postman');
const config = require('config');

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

// db
const users = {};

// nats.io
const namespace = 'user_service';
const NATS_HOST = config.get('nats.host');
const NATS_PORT = config.get('nats.port');
const pc = postman.createClient({'url': `nats://${NATS_HOST}:${NATS_PORT}`}, namespace);

// event
const event = {
    USER_CREATED: `${namespace}.user.created`
};

app.post('/register', (req, res) => {
    const {email, password} = req.body;

    if (!email || !password) return res.send('Thiếu email hoặc password');

    const newUser = {email, password};
    users[email] = newUser;
    pc.publish(event.USER_CREATED, newUser);

    res.send({success: true, new_user: newUser});
});

app.listen(3000, (err) => {
    if (!err) {
        console.log('user-service is listening on 3000');
    }
});